package mensajes.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion_db {

    Connection conn = null;

    public Conexion_db() {
        try {
            //Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/mensajes_db";
            String user = "postgres";
            String pass = "123";
            conn = DriverManager.getConnection(url, user, pass);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            System.out.println("Conexion Exitosa");
        }
    }
}
